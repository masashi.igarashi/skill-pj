### 変更履歴

|日付|バージョン|説明|
|----|----|----|
|yyyy/MM/dd|1.0|第１版を作成|


## 概要

Googleフォームを使うことで、色々なテストやクイズを作成することができます。

例えば、以下のようなテストやクイズが作れます。

* ３択クイズ
* 新入社員のための業務テスト
* 学校や教室で、生徒のための理解度チェックのテスト
* クライアント向けの商品・サービスの利点クイズ
* ブログで簡易的なクイズ

また、選択式、チェックボックス、プルダウンなどの質問形式では、正解に基づいて自動的に採点することが可能です。

!!!summary
    採点した結果を自動的にgoogle スプレッドシートに収集する機能もあるので、可能な限り、面談実施者による採点結果等もgoogleフォームを用意することを推奨します。

## 新しいテストと解答集を作成する

まず、最初に「テスト」と「解答集」を新規で作成します。

### 新しいテストを作成する

新規のフォームはもちろん、既存のフォームを使うこともできます。

https://forms.google.com</a> にアクセスします。

新しいフォームアイコン「＋」をクリックします。

<img class="alignnone size-full wp-image-3430" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-001.jpg" alt="Googleフォームで「＋」をクリックする" width="1284" height="353" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-001.jpg 1284w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-001-600x165.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-001-460x126.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-001-768x211.jpg 768w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-001-1024x282.jpg 1024w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-001-980x269.jpg 980w" sizes="(max-width: 1284px) 100vw, 1284px">

右上の《設定アイコン》をクリックします。

<img class="alignnone size-full wp-image-3431" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-002.jpg" alt="右上の設定アイコンをクリックします。" width="419" height="175" scale="0"></p>

《テスト》をクリックします。

<img class="alignnone size-full wp-image-3432" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-003.jpg" alt="《テスト》をクリックします。" width="644" height="724" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-003.jpg 644w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-003-600x675.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-003-409x460.jpg 409w" sizes="(max-width: 644px) 100vw, 644px">

《テストにする》をクリックします。

<img class="alignnone size-full wp-image-3433" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-004.jpg" alt="《テストにする》をクリックします。" width="624" height="328" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-004.jpg 624w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-004-600x315.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-004-460x242.jpg 460w" sizes="(max-width: 624px) 100vw, 624px">

《保存》をクリックします。

<img class="alignnone size-full wp-image-3434" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-005.jpg" alt="《保存》をクリックします。" width="624" height="453" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-005.jpg 624w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-005-600x436.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-005-460x334.jpg 460w" sizes="(max-width: 624px) 100vw, 624px">

### 解答集を作成する

グリッド形式以外であれば、全ての質問形式に点数を割り当てることができます。

質問を追加するには、**質問を追加アイコン「＋」追加 をクリック**します。

<img class="alignnone size-full wp-image-3435" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-006.jpg" alt="質問を追加するには、質問を追加アイコン「＋」追加 をクリックします。" width="947" height="529" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-006.jpg 947w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-006-600x335.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-006-460x257.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-006-768x429.jpg 768w" sizes="(max-width: 947px) 100vw, 947px">

#### 「質問」と「解答」を入力し、追加していきます。

<img class="alignnone size-full wp-image-3436" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-007.jpg" alt="質問と解答を入力します。" width="874" height="664" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-007.jpg 874w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-007-600x456.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-007-460x349.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-007-768x583.jpg 768w" sizes="(max-width: 874px) 100vw, 874px">

左下の《解答集を作成》をクリックします。

<img class="alignnone size-full wp-image-3437" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-008.jpg" alt="左下の《解答集を作成》をクリックします。" width="874" height="414" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-008.jpg 874w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-008-600x284.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-008-460x218.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-008-768x364.jpg 768w" sizes="(max-width: 874px) 100vw, 874px">

**《正解》を選択**します。

**質問の右上で、質問の《点数》を入力**します。

質問や回答オプションを編集するには、**《問題を編集》をクリック**します。

<img class="alignnone size-full wp-image-3438" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-009.jpg" alt="回答、点数を設定していく" width="804" height="454" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-009.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-009-600x339.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-009-460x260.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-009-768x434.jpg 768w" sizes="(max-width: 804px) 100vw, 804px">

### 解答に説明を追加する

解答にリンク、動画、ウェブサイトなどの詳しい説明を追加することができます。回答者がテストを完了すると、このフィードバックが表示されます。

質問をクリックし、**《解答集を作成》をクリック**します。

<img class="alignnone size-full wp-image-3437" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-008.jpg" alt="左下の《解答集を作成》をクリックします。" width="874" height="414" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-008.jpg 874w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-008-600x284.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-008-460x218.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-008-768x364.jpg 768w" sizes="(max-width: 874px) 100vw, 874px">

**《回答に対するフィードバックを追加》をクリック**します。

<img class="alignnone size-full wp-image-3440" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-011.jpg" alt="《回答に対するフィードバックを追加》をクリックします。" width="384" height="314" scale="0">

**《不正解》のフィードバックを入力**する。

<img class="alignnone size-full wp-image-3441" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-012.jpg" alt="不正解のフィードバックを入力する" width="624" height="314" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-012.jpg 624w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-012-600x302.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-012-460x231.jpg 460w" sizes="(max-width: 624px) 100vw, 624px">

*《正解》タブをクリックする。
*フィードバックを入力する。
*《保存》をクリックする。

<img class="alignnone size-full wp-image-3442" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-013.jpg" alt="正解のフィードバックを入力する" width="624" height="314" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-013.jpg 624w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-013-600x302.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-013-460x231.jpg 460w" sizes="(max-width: 624px) 100vw, 624px">

フィードバックの設定結果が反映されます。

<img class="alignnone size-full wp-image-3443" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-014.jpg" alt="フィードバックを入力して《保存》をクリックします。" width="784" height="664" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-014.jpg 784w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-014-600x508.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-014-460x390.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-014-768x650.jpg 768w" sizes="(max-width: 784px) 100vw, 784px">

### 回答の送信後に回答者に表示される項目を指定する

解答の送信中や送信後にユーザーに表示される項目を指定できます。

* 不正解だった質問
* 正解
* 点数

項目を変更するには:
**右上の《設定アイコン》をクリック**します。

<img class="alignnone size-full wp-image-3431" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-002.jpg" alt="右上の設定アイコンをクリックします。" width="419" height="175" scale="0">

《テスト》をクリックします。

※画像は、《テストにする前》の画像です。

<img class="alignnone size-full wp-image-3432" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-003.jpg" alt="《テスト》をクリックします。" width="644" height="724" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-003.jpg 644w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-003-600x675.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-003-409x460.jpg 409w" sizes="(max-width: 644px) 100vw, 644px">


* 《回答者が表示できる項目》で該当する項目の横のチェックボックスをオンにします。
* 《保存》をクリックします。

<img class="alignnone size-full wp-image-3445" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-015.jpg" alt="回答者が表示できる項目を選択し、保存をクリックする" width="600" height="650" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-015.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-015-425x460.jpg 425w" sizes="(max-width: 600px) 100vw, 600px">

## テストの回答を採点する

テストのすべての回答についての概要が自動的に作成されます。概要には次のような項目が含まれます。

* 誤答の多い質問
* 正解の数に関するグラフ
* スコアの平均、中央値、範囲

### テスト結果の概要を確認する

* 上部の《回答》をクリックする。
* 《概要》をクリックします。

<img class="alignnone size-full wp-image-3448" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-016.jpg" alt="上部の《回答》をクリックし、《概要》をクリックします。" width="804" height="604" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-016.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-016-600x451.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-016-460x346.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-016-768x577.jpg 768w" sizes="(max-width: 804px) 100vw, 804px">

個別の回答を表示するには、《個別》をクリックします。

<img class="alignnone size-full wp-image-3449" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-017.jpg" alt="個別の回答を表示するには、《個別》をクリックします。" width="804" height="604" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-017.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-017-600x451.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-017-460x346.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-017-768x577.jpg 768w" sizes="(max-width: 804px) 100vw, 804px">

### 個々の回答を採点する

メールアドレスを収集する場合は、個々の回答を採点してフィードバックを入力してから、結果を送信できます。回答の採点が終わったら、必ず変更を保存してください。

上部の《回答》をクリックし、《個別》をクリックします。

前後の回答に移動するには、《前へ》または《次へ》をクリックします。

<img class="alignnone size-full wp-image-3450" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-018.jpg" alt="次の回答を見る場合は《次へ》" width="804" height="354" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-018.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-018-600x264.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-018-460x203.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-018-768x338.jpg 768w" sizes="(max-width: 804px) 100vw, 804px"> <img class="alignnone size-full wp-image-3451" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-019.jpg" alt="前の回答を見る場合は《前へ》" width="804" height="354" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-019.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-019-600x264.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-019-460x203.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-019-768x338.jpg 768w" sizes="(max-width: 804px) 100vw, 804px">

* 採点する質問を見つけたら、質問の右上に《回答の点数》を入力します。
* 回答の下の《個別にフィードバックを追加》をクリックします。

<img class="alignnone size-full wp-image-3459" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-020-1.jpg" alt="回答の下の《個別にフィードバックを追加》をクリックします。" width="804" height="294" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-020-1.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-020-1-600x219.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-020-1-460x168.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-020-1-768x281.jpg 768w" sizes="(max-width: 804px) 100vw, 804px">

* フィードバックを入力します。
* 《保存》をクリックします。

<img class="alignnone size-full wp-image-3454" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-023.jpg" alt="フィードバックを入力して、《保存》をクリックします。" width="624" height="284" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-023.jpg 624w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-023-600x273.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-023-460x209.jpg 460w" sizes="(max-width: 624px) 100vw, 624px">

* 入力したフィードバックを確認する。
* 《保存》をクリックします。

<img class="alignnone size-full wp-image-3455" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-024.jpg" alt="フィードバックを入力して、《保存》をクリックします。" width="804" height="354" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-024.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-024-600x264.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-024-460x203.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-024-768x338.jpg 768w" sizes="(max-width: 804px) 100vw, 804px">

準備ができたら、点数を回答者にメールで送信できます。

!!!Warning
    「メールアドレスを収集する」設定にしておく設定は必須とします。手順は後述します。

### 結果をメールで送信する

フォーム内でメールアドレスを収集する場合は、準備が完了するまで結果の通知を保留することができます。

#### 手順１：メールアドレスを収集する

右上の設定アイコン設定をクリックします。

<img class="alignnone size-full wp-image-3431" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-002.jpg" alt="右上の設定アイコンをクリックします。" width="419" height="175" scale="0">

* 《全般》で《メールアドレスを収集する》にチェックをします。
* 《保存》をクリックします。

<img class="alignnone size-full wp-image-3453" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-021.jpg" alt="《全般》で《メールアドレスを収集する》にチェックする" width="624" height="624" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-021.jpg 624w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-021-100x100.jpg 100w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-021-600x600.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-021-150x150.jpg 150w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-021-460x460.jpg 460w" sizes="(max-width: 624px) 100vw, 624px">

#### 手順２：成績を表示するタイミングを選択する

右上の《設定アイコン》をクリックします。

* 《テスト》をクリックします。
* 次のいずれかを選択します。
* 送信直後：結果をすぐに回答者に知らせる場合はこのオプションを選択します。
* 手動で確認後：後で結果をメールで送信する場合はこのオプションを選択します。
* 《保存》をクリックします。

<img class="alignnone wp-image-3461 size-full" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-028.jpg" alt="成績を表示するタイミングを選択" width="624" height="664" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-028.jpg 624w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-028-600x638.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-028-432x460.jpg 432w" sizes="(max-width: 624px) 100vw, 624px">

#### 手順 3: 結果をメールで送信する

《回答》セクションから回答者にスコアをメールで送信する方法は、2つあります。

《概要》タブから《概要》をクリックし、《スコア》が表示されるまでスクロールします。

<img class="alignnone size-full wp-image-3462" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-029.jpg" alt="《概要》をクリックし、《スコア》が表示されるまでスクロールします。" width="804" height="374" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-029.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-029-600x279.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-029-460x214.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-029-768x357.jpg 768w" sizes="(max-width: 804px) 100vw, 804px">

《スコアの通知》をクリックします。

<img class="alignnone size-full wp-image-3463" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-030.jpg" alt="《スコアを通知》をクリックします。" width="804" height="314" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-030.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-030-600x234.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-030-460x180.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-030-768x300.jpg 768w" sizes="(max-width: 804px) 100vw, 804px">

* 送信する相手の横にある《チェックボックス》をオンにします。
* 《メールでスコアを通知》をクリックします。

<img class="alignnone size-full wp-image-3464" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-031.jpg" alt="送信する相手の横にあるチェックボックスをオンにします。《メールでスコアを通知》をクリックします。" width="604" height="374" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-031.jpg 604w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-031-600x372.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-031-460x285.jpg 460w" sizes="(max-width: 604px) 100vw, 604px">

* 《個別》タブから《個別》をクリックします。

<img class="alignnone size-full wp-image-3449" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-017.jpg" alt="個別の回答を表示するには、《個別》をクリックします。" width="804" height="604" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-017.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-017-600x451.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-017-460x346.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-017-768x577.jpg 768w" sizes="(max-width: 804px) 100vw, 804px">

右上の《スコアをリリース》をクリックします。

<img class="alignnone size-full wp-image-3456" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-025.jpg" alt="右上の《スコアをリリース》をクリックします。" width="804" height="564" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-025.jpg 804w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-025-600x421.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-025-460x323.jpg 460w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-025-768x539.jpg 768w" sizes="(max-width: 804px) 100vw, 804px">

* 送信する相手の横にある《チェックボックス》をオンにします。
* 《メールでスコアを通知》をクリックします。

<img class="alignnone size-full wp-image-3464" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-031.jpg" alt="送信する相手の横にあるチェックボックスをオンにします。《メールでスコアを通知》をクリックします。" width="604" height="374" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-031.jpg 604w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-031-600x372.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-031-460x285.jpg 460w" sizes="(max-width: 604px) 100vw, 604px">

すると、回答してくれた相手にメールが届きます。

<img class="alignnone size-full wp-image-3458" src="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-027.jpg" alt="すると、回答してくれた相手にメールが届きます。" width="682" height="328" srcset="https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-027.jpg 682w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-027-600x289.jpg 600w, https://www.ec-create.jp/cmswp/wp-content/uploads/2018/01/google-form-test-sakusei-saiten-027-460x221.jpg 460w" sizes="(max-width: 682px) 100vw, 682px">
