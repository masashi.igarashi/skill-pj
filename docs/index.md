# スキプロ運用ノート

本サイトは、markdown形式でのコンテンツを簡単にメンテナンスおよび運用するために発行しています。

**mkdocs**というツールを利用しています。

詳細は [mkdocs.org](https://mkdocs.org).をご参照ください。

## Commands

ローカルでコンテンツを作成・確認する場合には、プロジェクトルートフォルダから以下コマンドを利用します。


* `mkdocs new [dir-name]` - 新しいmkdocsプロジェクトを作成
* `mkdocs serve` - live-reloading （編集結果が即時反映される状態）でdocs serverを起動.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

!!!Warning
	python、mkdocsの各種パッケージインストール済みの環境でのみ、実行可能です。

!!!Information
	単体でのmarkdown形式でのドキュメント作成は、様々なツールが提供されています。Visual Studio Codeに任意の拡張機能を入れて使うとカスタマイズ性も高く便利です。

## Project layout

以下のフォルダ構成で運用するものとします。
画像ファイル等の各コンテンツに必要な付帯ファイルは、必ず設置コンテンツと同一階層配下に設置してください。

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and 
        operation	# 運用ガイド類を配置
        	xxx.md		
        develop		# 技術教材類を配置
        	yyy.md
        business	# マネジメント教材類を配置
        	zzz.md
        other files.

!!!Warning
	mkdocsでのドキュメントページは、目次に記載した内容が「#」相当の見出しとして出力されます。各ドキュメントページは「#」の見出しから開始するようお願いします。
	